package mail

type EmailService interface {
	SendMail() error
}