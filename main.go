package main

import (
	"fmt"
	"log"

	rabbitmqService "gitlab.com/kaidemy/common/services/rabbitmq"
	service "gitlab.com/kaidemy/mail-service/service"
	"gitlab.com/kaidemy/mail-service/utils"
)

func main() {
	conn, err := rabbitmqService.ConnectRabbitMQ()
	fmt.Println("Connect RabbitMQ success")
	if err != nil {
		log.Fatal(err)
	}
	utils.InitConfigurations()
	var forever chan struct{}
	
	go service.ReceiveForgotPassword(conn)

	<- forever
	defer conn.Close()
}