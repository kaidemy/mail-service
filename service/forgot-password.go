package service

import (
	"fmt"
	"html/template"
	"log"
	"net/smtp"
	"strings"

	amqp "github.com/rabbitmq/amqp091-go"
	rabbitmqCommom "gitlab.com/kaidemy/common/services/rabbitmq"
	mail "gitlab.com/kaidemy/mail-service/mail"
	"gitlab.com/kaidemy/mail-service/utils"
	"gitlab.com/kaidemy/mail-service/constants"
	constantsCommon "gitlab.com/kaidemy/common/constants"

)

// type InfoForgotPassword struct {
// 	FullName string
// 	Token    string
// }

type MailForgotPassword struct {
	to   []string
	data constantsCommon.MailInfoForgotPassword
}

func NewMailForgotPassword(to []string, data constantsCommon.MailInfoForgotPassword) mail.EmailService {
	return &MailForgotPassword{
		to,
		data,
	}
}


func ReceiveForgotPassword(conn *amqp.Connection) error {
	ch, err := conn.Channel()

	if err != nil {
		return nil
	}

	q, err := ch.QueueDeclare(
		rabbitmqCommom.SEND_MAIL_FORGOT_PASSWORD, // name
		false,                                    // durable
		false,                                    // delete when unused
		false,                                    // exclusive
		false,                                    // no-wait
		nil,                                      // arguments
	)

	if err != nil {
		return nil
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil {
		return nil
	}

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
			fakeData := constantsCommon.MailInfoForgotPassword {
				FullName: "TESTER",
				URLRedirect: "http://localhost:3000",
			}
			mailTo := "quangnam11032001@gmail.com"
			mailForgotPassword := NewMailForgotPassword([]string{mailTo}, fakeData)
			fmt.Println(fakeData)
			if err := mailForgotPassword.SendMail(); err != nil {
				d.Reject(true)
			} else {
				d.Ack(false)
			}
		}
	}()
	return nil
}

func (mail *MailForgotPassword) SendMail() error {
	from := utils.EnvConfigs.EmailSender

	password := utils.EnvConfigs.PasswordSender
	address :=  fmt.Sprintf("%s:%s", utils.EnvConfigs.HostSMTP, utils.EnvConfigs.PortSMTP)
	
	subject := constants.SUBJECT_RESET_PASSWORD

	var messageTemplate *template.Template

	template, err := template.ParseFiles("template/forgot-password.html")
	messageTemplate = template
	if err != nil {
		return err
	}

	var messageText strings.Builder

	if err := messageTemplate.Execute(&messageText, mail.data); err != nil {
		return err
	}

	message := []byte(fmt.Sprintf("To: %s\r\nSubject: %s\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n%s", strings.Join([]string{from}, ","), subject, messageText.String()))
	auth := smtp.PlainAuth("", from, password, utils.EnvConfigs.HostSMTP)

	if err := smtp.SendMail(address, auth, from, mail.to, message); err != nil {
		return err
	}

	return nil
}
