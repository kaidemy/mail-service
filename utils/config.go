package utils

import (
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
)

type Configurations struct {
	EmailSender    string
	PasswordSender string
	HostSMTP       string
	PortSMTP       string
}

var EnvConfigs *Configurations

func InitConfigurations() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	viper.AutomaticEnv()

	configs := &Configurations{
		EmailSender:    viper.GetString("EMAIL_SENDER"),
		PasswordSender: viper.GetString("PASSWORD_SENDER"),
		HostSMTP:       viper.GetString("HOST_SMTP"),
		PortSMTP:       viper.GetString("PORT_SMTP"),
	}
	EnvConfigs = configs
}
